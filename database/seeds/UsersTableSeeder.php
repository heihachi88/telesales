<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create administrator
        DB::table('users')->insert([
            'user_name' => 'Administrator',
            'user_email' => 'admin@telesales.test',
            'user_address' => 'Gagarina 34',
            'user_phone' => '+987823214',
            'password' => bcrypt('qweasdzxc'),
            'remember_token' => str_random(10)
        ]);

        // generating users for testing purposes with faker
        factory(App\User::class, 10)->create();
    }
}
