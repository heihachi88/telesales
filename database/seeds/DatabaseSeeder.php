<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(GoodsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(AdditionalsTableSeeder::class);
    }
}
