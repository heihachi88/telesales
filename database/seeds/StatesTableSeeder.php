<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create default states
        DB::table('states')->insert([
            [
                'state_id' => 1,
                'state_name' => 'Новый',
                'state_slug' => 'new',
            ],
            [
                'state_id' => 2,
                'state_name' => 'В работе',
                'state_slug' => 'on_operator',
            ],
            [
                'state_id' => 3,
                'state_name' => 'Подтвержден',
                'state_slug' => 'accepted',
            ],
        ]);
    }
}
