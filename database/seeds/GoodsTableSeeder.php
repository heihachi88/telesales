<?php

use Illuminate\Database\Seeder;

class GoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create default states
        DB::table('goods')->insert([
            [
                'good_id' => 1,
                'good_name' => 'Часы',
                'good_price' => 400,
            ],
            [
                'good_id' => 2,
                'good_name' => 'Детский планшет',
                'good_price' => 550,
            ],
        ]);
    }
}
