<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create default states
        DB::table('services')->insert([
            [
                'services_id' => 1,
                'services_name' => 'Уборка',
                'services_price' => 1400,
            ],
            [
                'services_id' => 2,
                'services_name' => 'Стирка',
                'services_price' => 550,
            ],s
        ]);
    }
}
