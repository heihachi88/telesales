<?php

use Illuminate\Database\Seeder;

class AdditionalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create default states
        DB::table('additionals')->insert([
            [
                'additional_id' => 1,
                'additional_name' => 'Курьер',
                'additional_price' => 100,
            ],
            [
                'additional_id' => 2,
                'additional_name' => 'Разгрузка',
                'additional_price' => 300,
            ],
        ]);
    }
}
