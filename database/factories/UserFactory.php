<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'user_name' => $faker->name,
        'user_email' => $faker->unique()->safeEmail,
        'user_address' => $faker->address,
        'user_phone' => $faker->phoneNumber,
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
    ];
});
