<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Orders
Route::resource('orders', 'OrderController');

// States
Route::resource('states', 'StateController');

// Goods
Route::resource('goods', 'GoodController');

// Services
Route::resource('services', 'ServiceController');

// Additional services
Route::resource('additionals', 'AdditionalController');

// Users
Route::resource('users', 'UserController');

// Item types
Route::get('item-type', 'HomeController@getType');