<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * override primary key
     */
    protected $primaryKey = 'order_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['order_created_at'];

    /**
     * Each order can have one state
     */
    public function state()
    {
        return $this->hasOne('App\State', 'state_id', 'order_state_id');
    }

    /**
     * Each order has one author
     */
    public function author()
    {
        return $this->hasOne('App\User', 'user_id', 'user_id');
    }

    /**
     * Each order has one good
     */
    public function good()
    {
        return $this->hasOne('App\Good', 'good_id', 'item_id');
    }

    /**
     * Each order has one service
     */
    public function service()
    {
        return $this->hasOne('App\Service', 'services_id', 'item_id');
    }

    /**
     * Each order has one additional service
     */
    public function additional()
    {
        return $this->hasOne('App\Additional', 'additional_id', 'item_id');
    }
}
