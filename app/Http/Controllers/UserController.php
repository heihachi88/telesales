<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        if ($request->ajax()) {
            $usrs = $user->newQuery();

            if ($request->filled('username')) {
                $usrs = $user->where('user_name', 'like', $request->query('username') . '%');
            }

            if ($request->filled('email')) {
                $usrs = $user->where('user_email', 'like', $request->query('email') . '%');
            }
            
            $users = $usrs->orderBy('user_name', 'asc')->paginate(10);

            // get full url with queryString
            $url = $request->fullUrl();

            $users_view = view('includes.users', compact('users'))->render();
            return response()->json(compact('users_view', 'url'));
        }

        // filtering products with query string parameters
        if ($request->query()) {
            $usrs = $user->newQuery();

            if ($request->filled('username')) {
                $usrs = $user->where('user_name', 'like', $request->query('username') . '%');
            }

            if ($request->filled('email')) {
                $usrs = $user->where('user_email', 'like', $request->query('email') . '%');
            }

            $users = $usrs->orderBy('user_name', 'asc')->paginate(10);
            // dd($request->query('order'));
            return view('users.index', compact('users'));
        }

        $users = User::orderBy('user_name', 'asc')->paginate(10);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;

        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string',
            'user_email' => 'required|email',
            'user_address' => 'required|string',
            'user_phone' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $user->user_name = $request->input('user_name');
        $user->user_email = $request->input('user_email');
        $user->user_address = $request->input('user_address');
        $user->user_phone = $request->input('user_phone');
        $user->password = bcrypt($request->input('password'));

        $user->save();

        alert()->success('Успех', 'Вы успешно добавили пользователя.');
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string',
            'user_email' => 'required|email',
            'user_address' => 'required|string',
            'user_phone' => 'required',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $user->user_name = $request->input('user_name');
        $user->user_email = $request->input('user_email');
        $user->user_address = $request->input('user_address');
        $user->user_phone = $request->input('user_phone');
        $user->password = bcrypt($request->input('password'));

        $user->save();

        alert()->success('Успех', 'Вы успешно обновили пользователя.');
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        
        $user->delete();

        return redirect()->action('UserController@index');
    }
}
