<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Good;
use Validator;

class GoodController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $goods = Good::paginate(10);
        return view('goods.index', compact('goods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('goods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $good = new Good;

        $validator = Validator::make($request->all(), [
            'good_name' => 'required',
            'good_price' => 'required|integer',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $good->good_name = $request->input('good_name');
        $good->good_price = $request->input('good_price');

        $good->save();

        alert()->success('Успех', 'Вы успешно добавили товар.');
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $good = Good::findOrFail($id);
        return view('goods.edit', compact('good'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $good = Good::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'good_name' => 'required',
            'good_price' => 'required|integer',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $good->good_name = $request->input('good_name');
        $good->good_price = $request->input('good_price');

        $good->save();

        alert()->success('Успех', 'Вы успешно обновили товар.');
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $good = Good::findOrFail($id);
        
        $good->delete();

        return redirect()->action('GoodController@index');
    }
}
