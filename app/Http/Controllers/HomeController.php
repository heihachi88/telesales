<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Good;
use App\Service;
use App\Additional;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Receive ajax get request to show correct item_type select list
     */
    public function getType(Request $request)
    {
        if ($request->ajax() && $request->has('type')) {
            switch ($request->query('type')) {
                case '':
                    $item_type = null;
                    $items = null;
                    break;
                case 'goods':
                    $item_type = 'goods';
                    $items = Good::get();
                    break;
                case 'services':
                    $item_type = 'services';
                    $items = Service::get();
                    break;
                case 'additionals':
                    $item_type = 'additionals';
                    $items = Additional::get();
                    break;
            }

            $items_view = view('includes.item_type', compact('item_type', 'items'))->render();
            return response()->json(compact('items_view', 'item_type'));
        }

        return abort(403, 'Unauthorized action.'); 
    }
}
