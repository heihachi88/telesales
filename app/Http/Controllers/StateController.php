<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use Validator;

class StateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::paginate(10);
        return view('states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $state = new State;

        $validator = Validator::make($request->all(), [
            'state_name' => 'required',
            'state_slug' => 'required',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $state->state_name = $request->input('state_name');
        $state->state_slug = $request->input('state_slug');

        $state->save();

        alert()->success('Успех', 'Вы успешно добавили статус заказа.');
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $state = State::findOrFail($id);
        return view('states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $state = State::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'state_name' => 'required',
            'state_slug' => 'required',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $state->state_name = $request->input('state_name');
        $state->state_slug = $request->input('state_slug');

        $state->save();

        alert()->success('Успех', 'Вы успешно добавили обнвили статус заказа.');
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $state = State::findOrFail($id);
        
        $state->delete();

        return redirect()->action('StateController@index');
    }
}
