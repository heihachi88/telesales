<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Validator;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::paginate(10);
        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new Service;

        $validator = Validator::make($request->all(), [
            'services_name' => 'required',
            'services_price' => 'required|integer',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $service->services_name = $request->input('services_name');
        $service->services_price = $request->input('services_price');

        $service->save();

        alert()->success('Успех', 'Вы успешно добавили услугу.');
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);
        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'services_name' => 'required',
            'services_price' => 'required|integer',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $service->services_name = $request->input('services_name');
        $service->services_price = $request->input('services_price');

        $service->save();

        alert()->success('Успех', 'Вы успешно обновили услугу.');
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        
        $service->delete();

        return redirect()->action('ServiceController@index');
    }
}
