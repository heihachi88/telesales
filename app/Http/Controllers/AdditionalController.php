<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Additional;
use Validator;

class AdditionalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $additionals = Additional::paginate(10);
        return view('additionals.index', compact('additionals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('additionals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $additional = new Additional;

        $validator = Validator::make($request->all(), [
            'additional_name' => 'required',
            'additional_price' => 'required|integer',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $additional->additional_name = $request->input('additional_name');
        $additional->additional_price = $request->input('additional_price');

        $additional->save();

        alert()->success('Успех', 'Вы успешно добавили услугу.');
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $additional = Additional::findOrFail($id);
        return view('additionals.edit', compact('additional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $additional = Additional::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'additional_name' => 'required',
            'additional_price' => 'required|integer',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $additional->additional_name = $request->input('additional_name');
        $additional->additional_price = $request->input('additional_price');

        $additional->save();

        alert()->success('Успех', 'Вы успешно обновили услугу.');
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $additional = Additional::findOrFail($id);
        
        $additional->delete();

        return redirect()->action('AdditionalController@index');
    }
}
