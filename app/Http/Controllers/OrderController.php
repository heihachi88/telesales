<?php

namespace App\Http\Controllers;

use App\Order;
use App\State;
use App\Good;
use App\Service;
use App\Additional;
use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('order_created_at', 'desc')->paginate(10);
        return view('orders.index', compact('orders', 'item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::get();
        $goods = Good::get();
        $services = Service::get();
        $additionals = Additional::get();
        return view('orders.create', compact('states', 'goods', 'services', 'additionals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;

        $validator = Validator::make($request->all(), [
            'states' => 'required|integer',
            'item_id' => 'required|integer',
            'item_type' => 'required|not_in:0',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $order->user_id = Auth::user()->user_id;
        $order->order_state_id = $request->input('states');
        $order->item_id = $request->input('item_id');
        $order->item_type = $request->input('item_type');
        $order->order_created_at = date('Y-m-d H:i:s');

        $order->save();

        alert()->success('Успех', 'Вы успешно добавили заказ.');
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $states = State::get();
        $order = Order::findOrFail($id);

        switch ($order->item_type) {
            case 'goods':
                $items = Good::get();
                break;
            case 'services':
                $items = Service::get();
                break;
            case 'additionals':
                $items = Additional::get();
                break;
        }

        return view('orders.edit', compact('order', 'states', 'items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'states' => 'required|integer',
            'item_id' => 'required|integer',
            'item_type' => 'required|not_in:0',
        ]);

        if ($validator->fails()) {
            alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withInput();
        }

        $order->order_state_id = $request->input('states');
        $order->item_id = $request->input('item_id');
        $order->item_type = $request->input('item_type');

        $order->save();

        alert()->success('Успех', 'Вы успешно обновили заказ.');
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        
        $order->delete();

        return redirect()->action('OrderController@index');
    }
}
