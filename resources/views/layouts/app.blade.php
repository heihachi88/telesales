<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->user_name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    @auth
                    <div class="col-md-3">
                        <ul class="nav flex-column">
                            <h4>{{ __('Navigation') }}</h4>

                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('/')) ? 'active' : '' }}" href="{{ url('/') }}">{{ __('Home') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('users*')) ? 'active' : '' }}" href="{{ action('UserController@index') }}">{{ __('Users') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('orders*')) ? 'active' : '' }}" href="{{ action('OrderController@index') }}">{{ __('Orders') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('states*')) ? 'active' : '' }}" href="{{ action('StateController@index') }}">{{ __('States') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('goods*')) ? 'active' : '' }}" href="{{ action('GoodController@index') }}">{{ __('Goods') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('services*')) ? 'active' : '' }}" href="{{ action('ServiceController@index') }}">{{ __('Services') }}</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('additionals*')) ? 'active' : '' }}" href="{{ action('AdditionalController@index') }}">{{ __('Additionals') }}</a>
                            </li>
                        </ul>
                    </div>
                    @endauth

                    <div class="col-md-9">
                        @yield('content')
                    </div>
                </div>
            </div>
        </main>
    </div>

    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
    @include('sweetalert::alert')
    @stack('scripts')
</body>
</html>
