@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Добавить статус</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('StateController@store') }}">
            @csrf

            <div class="form-group">
                <label for="state_name">Название статуса</label>
                <input type="text" class="form-control" id="state_name" name="state_name" aria-describedby="state_nameHelp" value="{{ old('state_name') }}">
            </div>

            <div class="form-group">
                <label for="state_slug">Slug</label>
                <input type="text" class="form-control" id="state_slug" name="state_slug" aria-describedby="state_slugHelp" value="{{ old('state_slug') }}">
            </div>

            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
</div>
@endsection