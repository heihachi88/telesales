@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Изменить статус {{ $state->state_id }}</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('StateController@update', ['id' => $state->state_id]) }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="state_name">Название статуса</label>
                <input type="text" class="form-control" id="state_name" name="state_name" aria-describedby="state_nameHelp" value="{{ $state->state_name }}">
            </div>

            <div class="form-group">
                <label for="state_slug">Slug</label>
                <input type="text" class="form-control" id="state_slug" name="state_slug" aria-describedby="state_slugHelp" value="{{ $state->state_slug }}">
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
</div>
@endsection