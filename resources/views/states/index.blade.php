@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <h4 class="mb-0 mt-2 float-left">Статусы заказов</h4> 
        <a href="{{ action('StateController@create') }}" class="btn btn-success float-right active" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Добавить статус</a>
    </div>

    <div class="card-body">
       <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название статуса</th>
                    <th scope="col">Slug</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($states as $state)
                <tr>
                    <th scope="row">{{ $state->state_id }}</th>
                    <td>{{ $state->state_name }}</td>
                    <td>{{ $state->state_slug }}</td>
                    <td>
                        <a href="{{ action('StateController@edit', ['id' => $state->state_id]) }}" class="btn btn-outline-dark"><i class="fas fa-pencil-alt"></i></a>
                        <form id="delete-req" style="display:inline;" method="POST" action="{{ action('StateController@destroy', ['id' => $state->state_id]) }}">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-outline-danger btn-delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $states->links() }}
    </div>
</div>
@endsection

@push('scripts')
    <script>
	$('.btn-delete').click(function(e) {
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Вы уверены?",
			text: "Вы не сможете восстановить этот статус!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Да, удалить его!",
			cancelButtonText: "Отмена"
        }).then((result) => {
            if (result.value) {
                form.submit();
            }
        });
	});
    </script>
@endpush