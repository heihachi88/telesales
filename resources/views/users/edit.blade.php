@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Редактировать пользователя {{ $user->user_id }}</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('UserController@update', ['id' => $user->user_id]) }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="user_name">Имя пользователя</label>
                <input type="text" class="form-control" id="user_name" name="user_name" aria-describedby="user_nameHelp" value="{{ $user->user_name }}">
            </div>

            <div class="form-group">
                <label for="user_email">Email</label>
                <input type="text" class="form-control" id="user_email" name="user_email" aria-describedby="user_emailHelp" value="{{ $user->user_email }}">
            </div>

            <div class="form-group">
                <label for="user_address">Адрес</label>
                <input type="text" class="form-control" id="user_address" name="user_address" aria-describedby="user_addressHelp" value="{{ $user->user_address }}">
            </div>

            <div class="form-group">
                <label for="user_phone">Телефон</label>
                <input type="text" class="form-control" id="user_phone" name="user_phone" aria-describedby="user_phoneHelp" value="{{ $user->user_phone }}">
            </div>

            <div class="form-group">
                <label for="password">Пароль</label>
                <input type="password" class="form-control" id="password" name="password" aria-describedby="passwordHelp">
            </div>

            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
</div>
@endsection