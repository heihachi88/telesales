@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <h4 class="mb-0 mt-2 float-left">Пользователи</h4> 
        <a href="{{ action('UserController@create') }}" class="btn btn-success float-right active" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Добавить пользователя</a>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="username">Имя пользователя</label>
                    <input type="text" class="form-control" id="username" name="username" aria-describedby="usernameHelp" value="{{ (Request::query('username')) ? Request::query('username') : '' }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="{{ (Request::query('email')) ? Request::query('email') : '' }}">
                </div>
            </div>
        </div>

        <div id="users_loader">
            @include('includes.users')
        </div>

    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('#username, #email').keyup(function () {
            var ajax_username = $('#username').val();
            var ajax_email = $('#email').val();

            $.ajax({
                method: 'get',
                url: '/users',
                data: {
                    username: ajax_username,
                    email: ajax_email
                },
                success: function(data) {
                    if (data.users_view) {
                        $('#users_loader').html(data.users_view);
                        window.history.pushState("", "", data.url);
                    } else {
                        $('#users_loader').html('');
                    }
                    
                    // console.log(data.url);
                },
                error: function(jqxhr, status, exception) {
                    alert('Error ' + exception);
                }
            });
        });

        $('.btn-delete').click(function(e) {
            e.preventDefault();
            var form = $(this).parents('form');
            swal({
                title: "Вы уверены?",
                text: "Вы не сможете восстановить этого пользователя!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Да, удалить его!",
                cancelButtonText: "Отмена"
            }).then((result) => {
                if (result.value) {
                    form.submit();
                }
            });
        });
    </script>
@endpush