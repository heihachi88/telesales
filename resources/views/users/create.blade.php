@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Добавить пользователя</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('UserController@store') }}">
            @csrf

            <div class="form-group">
                <label for="user_name">Имя пользователя</label>
                <input type="text" class="form-control" id="user_name" name="user_name" aria-describedby="user_nameHelp" value="{{ old('user_name') }}">
            </div>

            <div class="form-group">
                <label for="user_email">Email</label>
                <input type="text" class="form-control" id="user_email" name="user_email" aria-describedby="user_emailHelp" value="{{ old('user_email') }}">
            </div>

            <div class="form-group">
                <label for="user_address">Адрес</label>
                <input type="text" class="form-control" id="user_address" name="user_address" aria-describedby="user_addressHelp" value="{{ old('user_address') }}">
            </div>

            <div class="form-group">
                <label for="user_phone">Телефон</label>
                <input type="text" class="form-control" id="user_phone" name="user_phone" aria-describedby="user_phoneHelp" value="{{ old('user_phone') }}">
            </div>

            <div class="form-group">
                <label for="password">Пароль</label>
                <input type="password" class="form-control" id="password" name="password" aria-describedby="passwordHelp" value="{{ old('password') }}">
            </div>

            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
</div>
@endsection