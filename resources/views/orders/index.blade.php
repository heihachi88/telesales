@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <h4 class="mb-0 mt-2 float-left">Заказы</h4> 
        <a href="{{ action('OrderController@create') }}" class="btn btn-success float-right active" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Добавить заказ</a>
    </div>

    <div class="card-body">
       <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Автор</th>
                    <th scope="col">Товар</th>
                    <th scope="col">Тип товара</th>
                    <th scope="col">Дата создания</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($orders as $order)
                <tr>
                    <th scope="row">{{ $order->order_id }}</th>
                    <td>{{ $order->state->state_name }}</td>
                    <td>{{ $order->author->user_name }}</td>
                    <td>
                        @if ($order->item_type == 'goods')
                        {{ $order->good->good_name }}
                        @endif

                        @if ($order->item_type == 'services')
                        {{ $order->service->services_name }}
                        @endif

                        @if ($order->item_type == 'additionals')
                        {{ $order->additional->additional_name }}
                        @endif
                    </td>
                    <td>{{ $order->item_type }}</td>
                    <td>{{ $order->order_created_at->format('Y-d-m') }}</td>
                    <td>
                        <a href="{{ action('OrderController@edit', ['id' => $order->order_id]) }}" class="btn btn-outline-dark"><i class="fas fa-pencil-alt"></i></a>
                        <form id="delete-req" style="display:inline;" method="POST" action="{{ action('OrderController@destroy', ['id' => $order->order_id]) }}">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-outline-danger btn-delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $orders->links() }}
    </div>
</div>
@endsection

@push('scripts')
    <script>
	$('.btn-delete').click(function(e) {
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Вы уверены?",
			text: "Вы не сможете восстановить этот заказ!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Да, удалить его!",
			cancelButtonText: "Отмена"
        }).then((result) => {
            if (result.value) {
                form.submit();
            }
        });
	});
    </script>
@endpush