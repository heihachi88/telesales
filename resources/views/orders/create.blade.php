@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Добавить заказ</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('OrderController@store') }}">
            @csrf

            <div class="form-group">
                <label for="states">Статус заказа</label>
                <select class="form-control" id="states" name="states">
                    @foreach($states as $state)
                        <option value="{{ $state->state_id }}">{{ $state->state_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="item_type">Тип товара</label>
                <select class="form-control" id="item_type" name="item_type">
                    <option value="">Выберите</option>
                    <option value="goods">Goods</option>
                    <option value="services">Services</option>
                    <option value="additionals">Additionals</option>
                </select>
            </div>

            <div id="items_loader"></div>

            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#item_type').change(function () {
            var item_type = $(this).val();

            $.ajax({
                method: 'get',
                url: '/item-type',
                data: {
                    type: item_type
                },
                success: function(data) {
                    if (data.item_type != null) {
                        $('#items_loader').html(data.items_view);
                    } else {
                        $('#items_loader').html('');
                    }
                    
                    // console.log(data);
                },
                error: function(jqxhr, status, exception) {
                    alert('Error ' + exception);
                }
            });
        });
    });
</script>
@endpush