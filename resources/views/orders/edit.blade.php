@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Редактировать заказ {{ $order->order_id }}</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('OrderController@update', ['id' => $order->order_id]) }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="states">Статус заказа</label>
                <select class="form-control" id="states" name="states">
                    @foreach($states as $state)
                        <option value="{{ $state->state_id }}" {{ ($order->order_state_id == $state->state_id) ? 'selected' : '' }}>{{ $state->state_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="item_type">Тип товара</label>
                <select class="form-control" id="item_type" name="item_type">
                    <option value="">Выберите</option>
                    <option value="goods" {{ ($order->item_type == 'goods') ? 'selected' : '' }}>Goods</option>
                    <option value="services {{ ($order->item_type == 'services') ? 'selected' : '' }}">Services</option>
                    <option value="additionals" {{ ($order->item_type == 'additionals') ? 'selected' : '' }}>Additionals</option>
                </select>
            </div>

            <div id="items_loader">
                @include('includes.item_type', ['item_type' => $order->item_type])
            </div>

            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#item_type').change(function () {
            var item_type = $(this).val();

            $.ajax({
                method: 'get',
                url: '/item-type',
                data: {
                    type: item_type
                },
                success: function(data) {
                    if (data.item_type != null) {
                        $('#items_loader').html(data.items_view);
                    } else {
                        $('#items_loader').html('');
                    }
                    
                    // console.log(data);
                },
                error: function(jqxhr, status, exception) {
                    alert('Error ' + exception);
                }
            });
        });
    });
</script>
@endpush