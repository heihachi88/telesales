@if ($item_type == 'goods')
<div class="form-group">
    <label for="item_id">Товар</label>
    <select class="form-control" id="item_id" name="item_id">
        @foreach($items as $item)
            <option value="{{ $item->good_id }}"
                @if (isset($order))
                {{ ($order->item_id == $item->good_id) ? 'selected' : '' }}
                @endif
                >{{ $item->good_name }}</option>
        @endforeach
    </select>
</div>
@endif

@if ($item_type == 'services')
<div class="form-group">
    <label for="item_id">Услуга</label>
    <select class="form-control" id="item_id" name="item_id">
        @foreach($items as $item)
            <option value="{{ $item->services_id }}" 
                @if (isset($order))
                {{ ($order->item_id == $item->services_id) ? 'selected' : '' }}
                @endif
                >{{ $item->services_name }}</option>
        @endforeach
    </select>
</div>
@endif

@if ($item_type == 'additionals')
<div class="form-group">
    <label for="item_id">Дополнительная услуга</label>
    <select class="form-control" id="item_id" name="item_id">
        @foreach($items as $item)
            <option value="{{ $item->additional_id }}" 
                @if (isset($order))
                {{ ($order->item_id == $item->additional_id) ? 'selected' : '' }}
                @endif
                >{{ $item->additional_name }}</option>
        @endforeach
    </select>
</div>
@endif