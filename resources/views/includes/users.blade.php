<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя пользователя</th>
            <th scope="col">Email</th>
            <th scope="col">Адрес</th>
            <th scope="col">Телефон</th>
            <th scope="col">Действия</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($users as $user)
        <tr>
            <th scope="row">{{ $user->user_id }}</th>
            <td>{{ $user->user_name }}</td>
            <td>{{ $user->user_email }}</td>
            <td>{{ $user->user_address }}</td>
            <td>{{ $user->user_phone }}</td>
            <td>
                <a href="{{ action('UserController@edit', ['id' => $user->user_id]) }}" class="btn btn-outline-dark"><i class="fas fa-pencil-alt"></i></a>
                <form id="delete-req" style="display:inline;" method="POST" action="{{ action('UserController@destroy', ['id' => $user->user_id]) }}">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-outline-danger btn-delete"><i class="fas fa-trash-alt"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{{ $users->links() }}