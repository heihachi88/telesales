@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Редактировать услугу {{ $service->services_id }}</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('ServiceController@update', ['id' => $service->services_id]) }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="services_name">Наименование услуги</label>
                <input type="text" class="form-control" id="services_name" name="services_name" aria-describedby="services_nameHelp" value="{{ $service->services_name }}">
            </div>

            <div class="form-group">
                <label for="services_price">Стоимость</label>
                <input type="text" class="form-control" id="services_price" name="services_price" aria-describedby="services_priceHelp" value="{{ $service->services_price }}">
            </div>

            <button type="submit" class="btn btn-success">Обновить</button>
        </form>
    </div>
</div>
@endsection