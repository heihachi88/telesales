@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <h4 class="mb-0 mt-2 float-left">Услуги</h4> 
        <a href="{{ action('ServiceController@create') }}" class="btn btn-success float-right active" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Добавить услугу</a>
    </div>

    <div class="card-body">
       <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Наименование услуги</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($services as $service)
                <tr>
                    <th scope="row">{{ $service->services_id }}</th>
                    <td>{{ $service->services_name }}</td>
                    <td>{{ $service->services_price }}</td>
                    <td>
                        <a href="{{ action('ServiceController@edit', ['id' => $service->services_id]) }}" class="btn btn-outline-dark"><i class="fas fa-pencil-alt"></i></a>
                        <form id="delete-req" style="display:inline;" method="POST" action="{{ action('ServiceController@destroy', ['id' => $service->services_id]) }}">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-outline-danger btn-delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $services->links() }}
    </div>
</div>
@endsection

@push('scripts')
    <script>
	$('.btn-delete').click(function(e) {
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Вы уверены?",
			text: "Вы не сможете восстановить эту услугу!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Да, удалить ее!",
			cancelButtonText: "Отмена"
        }).then((result) => {
            if (result.value) {
                form.submit();
            }
        });
	});
    </script>
@endpush