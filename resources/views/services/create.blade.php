@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Добавить услугу</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('ServiceController@store') }}">
            @csrf

            <div class="form-group">
                <label for="services_name">Наименование услуги</label>
                <input type="text" class="form-control" id="services_name" name="services_name" aria-describedby="services_nameHelp" value="{{ old('services_name') }}">
            </div>

            <div class="form-group">
                <label for="services_price">Стоимость</label>
                <input type="text" class="form-control" id="services_price" name="services_price" aria-describedby="services_priceHelp" value="{{ old('services_price') }}">
            </div>

            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
</div>
@endsection