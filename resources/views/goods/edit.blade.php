@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Редактировать товар {{ $good->good_name }}</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('GoodController@update', ['id' => $good->good_id]) }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="good_name">Наименование товара</label>
                <input type="text" class="form-control" id="good_name" name="good_name" aria-describedby="good_nameHelp" value="{{ $good->good_name }}">
            </div>

            <div class="form-group">
                <label for="good_price">Стоимость</label>
                <input type="text" class="form-control" id="good_price" name="good_price" aria-describedby="good_priceHelp" value="{{ $good->good_price }}">
            </div>

            <button type="submit" class="btn btn-success">Обновить</button>
        </form>
    </div>
</div>
@endsection