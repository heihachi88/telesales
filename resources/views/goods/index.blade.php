@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <h4 class="mb-0 mt-2 float-left">Товары</h4> 
        <a href="{{ action('GoodController@create') }}" class="btn btn-success float-right active" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Добавить товар</a>
    </div>

    <div class="card-body">
       <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название товара</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($goods as $good)
                <tr>
                    <th scope="row">{{ $good->good_id }}</th>
                    <td>{{ $good->good_name }}</td>
                    <td>{{ $good->good_price }}</td>
                    <td>
                        <a href="{{ action('GoodController@edit', ['id' => $good->good_id]) }}" class="btn btn-outline-dark"><i class="fas fa-pencil-alt"></i></a>
                        <form id="delete-req" style="display:inline;" method="POST" action="{{ action('GoodController@destroy', ['id' => $good->good_id]) }}">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-outline-danger btn-delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $goods->links() }}
    </div>
</div>
@endsection

@push('scripts')
    <script>
	$('.btn-delete').click(function(e) {
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Вы уверены?",
			text: "Вы не сможете восстановить этот товар!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Да, удалить его!",
			cancelButtonText: "Отмена"
        }).then((result) => {
            if (result.value) {
                form.submit();
            }
        });
	});
    </script>
@endpush