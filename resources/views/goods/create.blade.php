@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Добавить товар</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('GoodController@store') }}">
            @csrf

            <div class="form-group">
                <label for="good_name">Наименование товара</label>
                <input type="text" class="form-control" id="good_name" name="good_name" aria-describedby="good_nameHelp" value="{{ old('good_name') }}">
            </div>

            <div class="form-group">
                <label for="good_price">Стоимость</label>
                <input type="text" class="form-control" id="good_price" name="good_price" aria-describedby="good_priceHelp" value="{{ old('good_price') }}">
            </div>

            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
</div>
@endsection