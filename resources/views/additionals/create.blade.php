@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Добавить доп.услугу</h4> 
    </div>

    <div class="card-body">
        <form method="POST" action="{{ action('AdditionalController@store') }}">
            @csrf

            <div class="form-group">
                <label for="additional_name">Наименование услуги</label>
                <input type="text" class="form-control" id="additional_name" name="additional_name" aria-describedby="additional_nameHelp" value="{{ old('additional_name') }}">
            </div>

            <div class="form-group">
                <label for="additional_price">Стоимость</label>
                <input type="text" class="form-control" id="additional_price" name="additional_price" aria-describedby="additional_priceHelp" value="{{ old('additional_price') }}">
            </div>

            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
</div>
@endsection