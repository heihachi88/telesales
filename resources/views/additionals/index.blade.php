@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <h4 class="mb-0 mt-2 float-left">Дополнительные услуги</h4> 
        <a href="{{ action('AdditionalController@create') }}" class="btn btn-success float-right active" role="button" aria-pressed="true"><i class="fas fa-plus"></i> Добавить доп.услугу</a>
    </div>

    <div class="card-body">
       <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Наименование услуги</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($additionals as $additional)
                <tr>
                    <th scope="row">{{ $additional->additional_id }}</th>
                    <td>{{ $additional->additional_name }}</td>
                    <td>{{ $additional->additional_price }}</td>
                    <td>
                        <a href="{{ action('AdditionalController@edit', ['id' => $additional->additional_id]) }}" class="btn btn-outline-dark"><i class="fas fa-pencil-alt"></i></a>
                        <form id="delete-req" style="display:inline;" method="POST" action="{{ action('AdditionalController@destroy', ['id' => $additional->additional_id]) }}">
                            @csrf
                            @method('DELETE')
                            <button type="button" class="btn btn-outline-danger btn-delete"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $additionals->links() }}
    </div>
</div>
@endsection

@push('scripts')
    <script>
	$('.btn-delete').click(function(e) {
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Вы уверены?",
			text: "Вы не сможете восстановить эту услугу!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Да, удалить ее!",
			cancelButtonText: "Отмена"
        }).then((result) => {
            if (result.value) {
                form.submit();
            }
        });
	});
    </script>
@endpush